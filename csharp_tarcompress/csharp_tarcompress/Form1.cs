﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ICSharpCode.SharpZipLib.Tar;
using ICSharpCode.SharpZipLib.GZip;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FolderBrowserDialog openfolderdialog = new FolderBrowserDialog();
            SaveFileDialog savedialog = new SaveFileDialog();

            openfolderdialog.ShowDialog();
            savedialog.ShowDialog();
            string folderpath = openfolderdialog.SelectedPath;
            string savepath = savedialog.FileName;
            CreateTarGZ(savepath + ".tar", folderpath);
        }
        private void CreateTarGZ(string tgzFilename, string sourceDirectory)
        {
            Stream outStream = File.Create(tgzFilename);
            Stream gzoStream = new GZipOutputStream(outStream);
            TarArchive tarArchive__1 = TarArchive.CreateOutputTarArchive(gzoStream);

            tarArchive__1.RootPath = sourceDirectory.Replace('\\', '/');
            if (tarArchive__1.RootPath.EndsWith("/"))
            {
                tarArchive__1.RootPath = tarArchive__1.RootPath.Remove(tarArchive__1.RootPath.Length - 1);
            }

            AddDirectoryFilesToTar(tarArchive__1, sourceDirectory, true);

            tarArchive__1.Close();
            System.Environment.Exit(0);
        }
        private void AddDirectoryFilesToTar(TarArchive tarArchive, string sourceDirectory, bool recurse)
        {
            TarEntry tarEntry__1 = TarEntry.CreateEntryFromFile(sourceDirectory);
            tarArchive.WriteEntry(tarEntry__1, false);
            string[] filenames = Directory.GetFiles(sourceDirectory);
            foreach (string filename in filenames)
            {
                tarEntry__1 = TarEntry.CreateEntryFromFile(filename);

                tarArchive.WriteEntry(tarEntry__1, true);
            }

            if (recurse)
            {
                string[] directories = Directory.GetDirectories(sourceDirectory);
                foreach (string directory__2 in directories)
                {
                    AddDirectoryFilesToTar(tarArchive, directory__2, recurse);
                }
            }
        }
       
    }
}
